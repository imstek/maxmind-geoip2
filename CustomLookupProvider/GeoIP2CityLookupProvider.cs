﻿using System;
using Sitecore.Analytics.Lookups;
using Sitecore.Diagnostics;
using Sitecore.Analytics.Model;
using log4net;
using Newtonsoft.Json;



namespace MaxMind.GeoIP2.CustomLookupProvider
{
    public class GeoIP2CityLookupProvider : LookupProviderBase
    {
       private static ILog log = Sitecore.Diagnostics.LoggerFactory.GetLogger("MaxMindGeoIP2LogFileAppender");

        public override WhoIsInformation GetInformationByIp(string ip)
        {
            WhoIsInformation information = new WhoIsInformation();

            try
            {

            #region  Declarations

                int id;
                bool intResBpool = Int32.TryParse(Sitecore.Configuration.Settings.GetSetting("MaxMindGeoIP2.ID"), out id);
                string key = Sitecore.Configuration.Settings.GetSetting("MaxMindGeoIP2.Key"); ;

                bool isDebug;
                if (!bool.TryParse(Sitecore.Configuration.Settings.GetSetting("MaxMindGeoIP2.isDebug"), out isDebug)){                                 
                    isDebug = false;
                }

                #endregion
      
            Assert.ArgumentNotNull(ip, "ip");

            #region MaxMid 

            if(isDebug) log.Info("MaxMind Service - init client");
           
            using (var client = new WebServiceClient(id, key))
            {
                MaxMind.GeoIP2.Responses.CityResponse response =  client.City(ip);
               
                information.City = response.City.Name;
                information.Country = response.Country.IsoCode;
                information.PostalCode = response.Postal.Code;
                information.Longitude = response.Location.Longitude;
                information.Latitude = response.Location.Latitude;
                information.Region = response.MostSpecificSubdivision.IsoCode;
                information.Isp = response.Traits.Isp;
                information.BusinessName = response.Traits.Organization;     
                information.MetroCode = response.Location.MetroCode.ToString();
                
                 if (isDebug) log.Info(String.Format("MaxMind Service - data:{0}", LogObject(response)));
                }

                if (isDebug) log.Info("MaxMind Service - search complte");
                #endregion

                return information;

            }
            catch (Exception ex)
            {
                log.Error("Exception occurred.  Exception: " + ex.Message);
            }


            return information;
        }
        public static string LogObject(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

    }
}