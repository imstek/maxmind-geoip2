﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="Sitecore.Analytics" %>
<%@ Import Namespace="Sitecore.Analytics.Pipelines.StartTracking" %>
<%@ Import Namespace="System.Net" %>



<script runat="server">



      protected void Page_Load(object sender, EventArgs e)
        {
            string ip = new IPAddress(
          Tracker.CurrentVisit.Ip).ToString();


            var w = Sitecore.Analytics.Lookups.LookupManager.GetInformationByIp(ip);
            ListBox2.Items.Clear();
            ListBox2.Items.Add("IP: " + ip);
            ListBox2.Items.Add("BusinessName: " + w.BusinessName);
            ListBox2.Items.Add("City: " + w.City);
            ListBox2.Items.Add("Country: " + w.Country);
            ListBox2.Items.Add("Isp: " + w.Isp);
            ListBox2.Items.Add("Latitude: " + w.Latitude);
            ListBox2.Items.Add("Longitude: " + w.Longitude);
            ListBox2.Items.Add("MetroCode: " + w.MetroCode);
            ListBox2.Items.Add("PostalCode: " + w.PostalCode);
            ListBox2.Items.Add("Region: " + w.Region);


        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            var w = Sitecore.Analytics.Lookups.LookupManager.GetInformationByIp(TextBox1.Text);
            ListBox1.Items.Clear();
            ListBox1.Items.Add("IP: " + TextBox1.Text);
            ListBox1.Items.Add("BusinessName: " + w.BusinessName);
            ListBox1.Items.Add("City: " + w.City);
            ListBox1.Items.Add("Country: " + w.Country);
            ListBox1.Items.Add("Isp: " + w.Isp);
            ListBox1.Items.Add("Latitude: " + w.Latitude);
            ListBox1.Items.Add("Longitude: " + w.Longitude);
            ListBox1.Items.Add("MetroCode: " + w.MetroCode);
            ListBox1.Items.Add("PostalCode: " + w.PostalCode);
            ListBox1.Items.Add("Region: " + w.Region);

        }
</script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
   <form id="form1" runat="server">
    <div>
        <asp:ListBox ID="ListBox2" runat="server" Height="314px" Width="557px"></asp:ListBox>


        <hr>


        <asp:TextBox ID="TextBox1" runat="server" Width="229px"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="GetInformartionByIP" Width="234px" />
    
        <br />
        <asp:ListBox ID="ListBox1" runat="server" Height="314px" Width="557px"></asp:ListBox>
    
    </div>
    </form>
</body>
</html>
